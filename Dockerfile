FROM ubuntu:20.04

LABEL repository="https://codeberg.org/FiliusPatris/pwnbox.git"
LABEL maintainer="FiliusPatris"

RUN apt-get update && \
	apt-get install -y git make gcc wget curl vim \
	python3 python3-pip \
	binutils strace ltrace

# DL & build Radare2
RUN git clone https://github.com/radareorg/radare2 && \
	cd radare2 && sys/install.sh --install && \
	cd ../ && rm -rf radare2 && r2pm init && r2pm upgrade;
