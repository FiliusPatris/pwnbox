#!/bin/sh

TAG=${1-20.04}; # Default tag: 20.04 (Ubuntu version)

RUNNING=$(docker ps -q --filter name=pwnbox);

if [ -z "$RUNNING" ]
then
	docker run -v $(pwd):/pwd --name pwnbox --rm -d -i filiuspatris/pwnbox:$TAG;
fi

docker exec -it pwnbox /bin/bash
